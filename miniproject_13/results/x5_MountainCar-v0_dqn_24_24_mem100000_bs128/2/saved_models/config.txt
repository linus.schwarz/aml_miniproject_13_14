dqn without dueling
Dueling Option: avg
PER: False
Batch Size: 128
Replay Memory size: 100000
Learning Rate: 0.001
Discount Factor: 0.99
Epsilon Decay Rate: 0.996
Train Start: 1000
