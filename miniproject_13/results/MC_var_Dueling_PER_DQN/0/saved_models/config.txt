dqn with avg dueling
Dueling Option: avg
PER: True
Batch Size: 32
Replay Memory size: 20000
Learning Rate: 0.001
Discount Factor: 0.99
Epsilon Decay Rate: 0.996
Train Start: 1000
