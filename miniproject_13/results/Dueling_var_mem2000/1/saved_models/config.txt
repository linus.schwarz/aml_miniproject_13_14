dqn with avg dueling
Dueling Option: avg
PER: False
Batch Size: 64
Replay Memory size: 2000
Learning Rate: 0.001
Discount Factor: 0.99
Epsilon Decay Rate: 0.996
Train Start: 1000
