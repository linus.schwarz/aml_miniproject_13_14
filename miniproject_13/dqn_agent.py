"""
LunarLander-v2 (dueling) DQN approach with tensorflow.
"""

import os
import gym
import sys
import random
import numpy as np
from tensorflow.keras.layers import Dense, Input, Lambda
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.utils import plot_model
from tensorflow.keras.optimizers import Adam
from collections import deque
import matplotlib.pyplot as plt
import tensorflow as tf
import pickle
import pandas as pd
from Memory import Memory
import time
from matplotlib import animation


class DQNAgent:
    def __init__(self,
                 env_name,
                 save_dir,
                 per=False,
                 memory_size=20000,
                 batch_size=24,
                 discount_factor=0.99,
                 learning_rate=0.001,
                 train_start=1000,
                 epsilon=1.0,
                 epsilon_decay_rate=0.996,
                 ddqn_flag=False,
                 polyak_avg=False,
                 pa_tau=0.1,
                 duel_flag=True,
                 dueling_option='avg',
                 load_weights_path=None,
                 load_exp_path=None,
                 hidden_layers=None,
                 activation=None):

        # create environment
        if hidden_layers is None:
            hidden_layers = [24, 24]
        if activation is None:
            activation = 'relu'

        self.env_name = env_name
        self.env = gym.make(env_name)
        self.save_dir = save_dir

        self.per = per

        self.state_size = self.env.observation_space.shape[0]
        self.action_size = self.env.action_space.n

        self.batch_size = batch_size
        self.memory_size = memory_size

        # Create replay memory to store experience

        # for prioritized experience replay
        if per:
            self.memory = Memory(capacity=memory_size)
        else:
            self.memory = deque(maxlen=self.memory_size)

        self.train_start = train_start
        self.learning_rate = learning_rate
        self.discount_factor = discount_factor
        self.epsilon = epsilon
        self.epsilon_decay_rate = epsilon_decay_rate
        self.ddqn = ddqn_flag
        self.dueling_option = dueling_option
        self.polyak_avg = polyak_avg
        self.pa_tau = pa_tau
        self.duel_flag = duel_flag
        self.hidden_layers = hidden_layers
        self.activation = activation

        self.optimizer = Adam(learning_rate=learning_rate)

        # create main model and target model
        self.model = self._build_model()
        self.target_model = self._build_model()

        if load_weights_path is not None:
            self.model.load_weights(load_weights_path)
            print('Weights loaded from File')

        if load_exp_path is not None:
            with open(load_exp_path, 'rb') as file:
                self.memory = pickle.load(file)
                print('Experience loaded from file')

        # initially both models share same weight
        self.target_model.set_weights(self.model.get_weights())
        # class constructor ends here

    def display_model_info(self):
        with open(f'results/{self.save_dir}/saved_models/config.txt', 'w') as f:
            f.write(f'dqn {"with" if self.duel_flag else "without"} dueling\n')
            if self.polyak_avg:
                f.write('Implements Polyak Averaging\n')
                f.write(f'PA Weighting factor, tau: {self.pa_tau}\n')
            f.write(f'Dueling Option: {self.dueling_option}\n')
            f.write(f'Prioritized experience replay: {self.per}\n')
            f.write(f'Batch Size: {self.batch_size}\n')
            f.write(f'Replay Memory size: {self.memory_size}\n')
            f.write(f'Learning Rate: {self.learning_rate}\n')
            f.write(f'Discount Factor: {self.discount_factor}\n')
            f.write(f'Epsilon Decay Rate: {self.epsilon_decay_rate}\n')
            f.write(f'Train Start: {self.train_start}\n')

    def _build_model(self):
        initializer = tf.keras.initializers.LecunUniform()

        # Advantage network
        network_input = Input(shape=(self.state_size,), name='network_input')

        prev_adv_layer = network_input
        for index in range(0, len(self.hidden_layers)):
            layer = Dense(self.hidden_layers[index], activation='relu', name=f'A{index}', kernel_initializer=initializer)(prev_adv_layer)
            prev_adv_layer = layer

        AN = Dense(self.action_size, activation='linear', name=f'A{len(self.hidden_layers)}')(prev_adv_layer)
        V3 = Dense(1, activation='linear', name='V3')(prev_adv_layer)

        if self.duel_flag:
            if self.dueling_option == 'avg':
                network_output = Lambda(lambda x: x[0] - tf.reduce_mean(x[0]) + x[1], output_shape=(self.action_size,),
                                        name='network_output')([AN, V3])
            elif self.dueling_option == 'max':
                network_output = Lambda(lambda x: x[0] - tf.reduce_mean(x[0]) + x[1], output_shape=(self.action_size,),
                                        name='network_output')([AN, V3])
            elif self.dueling_option == 'naive':
                network_output = Lambda(lambda x: x[0] + x[1], output_shape=(self.action_size,), name='network_output')(
                    [AN, V3])
            else:
                raise Exception('Invalid Dueling Option')
        else:  # normal model
            network_output = AN

        model = Model(network_input, network_output)
        model.summary()
        plot_model(model, to_file=f'results/{self.save_dir}/saved_models/model.png', show_shapes=True,
                   show_layer_names=True)
        return model

    def update_target_network(self):
        if self.ddqn and self.polyak_avg:
            weights = self.model.get_weights()
            target_weights = self.target_model.get_weights()
            for i in range(len(target_weights)):
                target_weights[i] = weights[i] * self.pa_tau + target_weights[i] * (1 - self.pa_tau)
            self.target_model.set_weights(target_weights)
        else:
            self.target_model.set_weights(self.model.get_weights())

    def update_epsilon(self):
        if self.epsilon > 0.01:
            self.epsilon *= self.epsilon_decay_rate
        else:
            self.epsilon = 0.01

    def add_experience(self, state, action, reward, next_state, done):
        if self.per:
            self.memory.store([state, action, reward, next_state, done])
        else:
            self.memory.append([state, action, reward, next_state, done])

    def select_action(self, state):
        if random.random() < self.epsilon:
            return np.random.randint(0, self.action_size)
        else:
            q_values = self.model(state)
            action = np.argmax(q_values[0])
            return action

    def get_maxQvalue_nextstate(self, next_state):
        # max Q value among the next state's action
        if self.ddqn:
            # DDQN
            # Current Q network selects the action
            # a'_max = argmax_a' Q(s',a')
            action = np.argmax(self.model.predict(next_state)[0])
            # target Q network evaluates the action
            # Q_max = Q_target(s', a'_max)
            max_q_value = self.target_model.predict(next_state)[0][action]
        else:
            # DQN chooses the max Q value among next actions
            # Selection and evaluation of action is on the target Q network
            # Q_max = max_a' Q_target(s', a')
            max_q_value = np.amax(self.target_model.predict(next_state)[0])

        return max_q_value

    def get_maxQvalue_nextstate_batch(self, next_states):
        q_values_next_state = self.target_model.predict(next_states)
        max_q_values = np.amax(q_values_next_state, axis=1)
        return max_q_values

    def train_model(self):
        t0 = time.time()

        if self.per:
            tree_idx, mini_batch = self.memory.sample(self.batch_size)
        else:
            if len(self.memory) < self.train_start: return 0
            mini_batch = random.sample(self.memory, self.batch_size)

        # Convert train inputs to tensors to avoid multiple function traces.
        states = tf.convert_to_tensor([i[0] for i in mini_batch], dtype=np.float32)
        actions = tf.convert_to_tensor([i[1] for i in mini_batch], dtype=np.int32)
        rewards = tf.convert_to_tensor([i[2] for i in mini_batch], dtype=np.float32)
        next_states = tf.convert_to_tensor([i[3] for i in mini_batch], dtype=np.float32)
        dones = tf.convert_to_tensor([i[4] for i in mini_batch], dtype=np.float32)

        # Perform training and return abs_error in case of PER on, else None.
        abs_errors = self.train_fast(states, actions, rewards, next_states, dones)

        if self.per:
            self.memory.batch_update(tree_idx, abs_errors)

        # update epsilon with each training step
        self.update_epsilon()

        total = time.time() - t0
        return total

    @tf.function
    def train_fast(self, states, actions, rewards, next_states, dones):
        states = tf.squeeze(states)
        next_states = tf.squeeze(next_states)

        with tf.GradientTape() as g:
            targets = rewards + tf.constant(self.discount_factor) * (
                tf.math.reduce_max(self.target_model(next_states), axis=1)) * (1 - dones)
            indices = tf.range(tf.constant(self.batch_size), dtype=np.int32)
            q_values = tf.gather_nd(self.model(states), tf.stack((indices, actions), -1))
            loss = tf.keras.losses.mse(targets, q_values)

        # Compute the gradients from the loss
        grads = g.gradient(loss, self.model.trainable_variables)
        # Apply the gradients to the model's parameters
        self.optimizer.apply_gradients(zip(grads, self.model.trainable_variables))

        # prioritized experience replay update step
        if self.per:
            absolute_errors = tf.math.abs(q_values - targets)
            return absolute_errors

    def plot_running_avg(self, tot_rewards):
        N = len(tot_rewards)
        running_avg = np.empty(N)
        for t in range(N):
            running_avg[t] = np.mean(tot_rewards[max(0, t - 100):(t + 1)])

        fig, ax = plt.subplots(figsize=(8, 6))

        ax.plot(running_avg)
        plt.title("running average of rewards", size=16)
        plt.xlabel('episode', size=14)
        plt.ylabel('reward', size=14)
        plt.rc('xtick', labelsize=14)  # fontsize of the tick labels
        plt.rc('ytick', labelsize=14)  # fontsize of the tick labels
        plt.savefig(f'results/{self.save_dir}/plots/running_avg.png')
        plt.show()

    def plot_training_reward(self, tot_rewards):
        # The reward for each training episode while training your agent?
        # The reward per trial for 100 trials using your trained agent?
        fig, ax = plt.subplots(figsize=(8, 6))

        ax.plot(tot_rewards)
        plt.title("reward for each training episode", size=16)
        plt.xlabel('episode', size=14)
        plt.ylabel('reward', size=14)
        plt.rc('xtick', labelsize=14)  # fontsize of the tick labels
        plt.rc('ytick', labelsize=14)  # fontsize of the tick labels
        plt.savefig(f'results/{self.save_dir}/plots/training_reward.png')
        plt.show()

    def run(self, solved_at, render=True, max_episodes=1000):
        target_network_update_freq = 10
        model_save_freq = 50
        render_freq = 1
        train = True

        total_episodes = []
        total_steps = []
        total_rewards = np.array([])

        for e in range(max_episodes):
            state = self.env.reset().reshape(1, self.state_size)
            done = False
            total_reward = 0
            steps = 0
            total = 0
            while not done:
                t1 = time.time()
                a = agent.select_action(state)
                next_state, reward, done, info = self.env.step(a)
                next_state = np.reshape(next_state, [1, self.state_size])

                if train:
                    # add experiences to replay memory
                    agent.add_experience(state, a, reward, next_state, done)

                    # experience replay - training
                    _ = agent.train_model()

                if e % render_freq == 0 and render:
                    still_open = self.env.render()
                    if not still_open: break

                steps += 1
                state = next_state
                total_reward += reward

                if steps % target_network_update_freq == 0 or done:
                    agent.update_target_network()

                t2 = time.time()
                total += t2 - t1
                if done: break

            print(f"episode time {total}")

            total_episodes.append(e)
            total_steps.append(steps)
            total_rewards = np.append(total_rewards, total_reward)

            avg_last_100 = total_rewards[max(0, e - 100):(e + 1)].mean()
            avg_last_10 = total_rewards[max(0, e - 10):(e + 1)].mean()

            print("======",
                  "episode:", e,
                  "total reward:", total_reward,
                  "avg reward (last 10):", avg_last_10,
                  "avg reward (last 100)", avg_last_100,
                  "iters:", steps,
                  "epsilon", self.epsilon,
                  "======")

            if e % model_save_freq == 0:
                log_df = pd.DataFrame(data={'episodes': total_episodes, 'steps': total_steps, 'rewards': total_rewards})
                log_df.to_csv(f'results/{self.save_dir}/logs/data.{e}.csv', index=False)
                self.model.save(f'results/{self.save_dir}/saved_models/dqn.{e}.h5')

            if avg_last_100 >= solved_at:
                print('The problem is solved in {} episodes. Exiting'.format(e))
                break

        print("avg reward for last 100 episodes:", total_rewards[-100:].mean())

        if train:
            log_df = pd.DataFrame(data={'episodes': total_episodes, 'steps': total_steps, 'rewards': total_rewards})
            log_df.to_csv(f'results/{self.save_dir}/logs/data.csv', index=False)
            self.model.save(f'results/{self.save_dir}/saved_models/dqn.h5')

        return total_rewards


mountain_car = 'MountainCar-v0'
lunar_lander = 'LunarLander-v2'

model_ext = ".h5"


def save_frames_as_gif(frames, path):

    plt.figure(figsize=(frames[0].shape[1] / 72.0, frames[0].shape[0] / 72.0), dpi=72)

    patch = plt.imshow(frames[0])
    plt.axis('off')

    def animate(i):
        patch.set_data(frames[i])

    anim = animation.FuncAnimation(plt.gcf(), animate, frames=len(frames), interval=0)
    anim.save(f'{path}.gif', writer='imagemagick', fps=15)


def run_test(environment_name, saved_model_root, episodes=2):
    epsilon = 1.0
    env = gym.make(environment_name)

    img_save_freq = 5

    state_size = env.observation_space.shape[0]
    action_size = env.action_space.n

    search_path = f'results/{saved_model_root}/saved_models/'
    file_names = get_filename_with_pattern(path=search_path, ext=model_ext)
    gif_save_path = f'{search_path}/gifs/'

    if not os.path.exists(gif_save_path):
        os.makedirs(gif_save_path)

    def select_action(s, m):
        q_values = m(s)
        action = np.argmax(q_values[0])
        return action

    for trained_model_name in file_names:
        trained_model_path = f'{search_path}/{trained_model_name}'
        model = load_model(trained_model_path, compile=False)
        frames = []

        for e in range(episodes):
            state = env.reset().reshape(1, state_size)
            done = False
            while not done:
                frame = env.render(mode='rgb_array')
                if e % img_save_freq == 0:
                    frames.append(frame)

                a = select_action(state, model)
                next_state, reward, done, info = env.step(a)
                next_state = np.reshape(next_state, [1, state_size])
                state = next_state

        # save_frames_as_gif(frames, path=f'{gif_save_path}/{trained_model_name[:-len(model_ext)]}')


def get_filename_with_pattern(path, ext):
    included_extensions = ['h5']
    filenames = [fn for fn in os.listdir(path) if any(fn.endswith(ext) for ext in included_extensions)]
    return filenames


if __name__ == "__main__":
    start = time.time()

    test = True

    if not test:

        # name of saved run
        save_timestamp = time.time()
        env_name = lunar_lander
        save_dir = f'v2_{env_name}_{save_timestamp}'

        # save paths
        if not os.path.exists(f"results/{save_dir}"):
            os.makedirs(f"results/{save_dir}/logs")
            os.makedirs(f"results/{save_dir}/plots")
            os.makedirs(f"results/{save_dir}/saved_models")

        # Create a DQN Agent
        agent = DQNAgent(
            env_name=env_name,
            duel_flag=True,
            per=False,
            memory_size=1000000,
            batch_size=64,
            save_dir=save_dir,
            hidden_layers=[150, 120],
            activation='relu'
        )

        if env_name == 'LunarLander-v2':
            solved_at = 200
        elif env_name == 'MountainCar-v0':
            solved_at = -110
        else:
            print("Unsuited environment, exiting...")
            sys.exit()

        agent.display_model_info()

        # Main loop
        total_rewards = agent.run(solved_at=solved_at, render=False, max_episodes=2000)

        # Plots
        agent.plot_training_reward(total_rewards)
        agent.plot_running_avg(total_rewards)

        print('total elapsed time:', time.time() - start)

    else:
        # testing
        paths = 'LunarLander-v2_dqn_150_120__1000000_64_solved_210'
        run_test(environment_name=lunar_lander, saved_model_root=paths, episodes=1)
