"""
Functions for building models (common and split) and automate testing of model architectures.
"""


import gym
from tensorflow import keras
from tensorflow.keras import layers

from dualpole import DualPole
from evaluation import train_model, evaluate_model
from visualization import render_episode, save_gif


def build_cartpole():
    def create_cartpole():
        return gym.make("CartPole-v1"), "CartPole-v1"
    return create_cartpole

def build_dualpole(end_on_touch=False, dist_penalty=False, fade_in_penalty=False):
    name = "DualPole"
    name += "_touch" if end_on_touch else ""
    name += "_penalty" if dist_penalty else ""
    name += "_fadein" if fade_in_penalty else ""

    def create_dualpole():
        return (DualPole(end_on_touch=end_on_touch,
                        dist_penalty=dist_penalty,
                        fade_in_penalty=fade_in_penalty),
                name)
    return create_dualpole

def build_model_common(common_layers=[128]):
    def create_model(num_states, num_actions):
        inputs = layers.Input(shape=(num_states,), name="input")

        hidden_common_layers = []
        for i, hidden_units in enumerate(common_layers):
            previous = inputs if i == 0 else hidden_common_layers[-1]
            hidden_common_layers.append(layers.Dense(hidden_units, activation="relu", name=f"common{i}")(previous))

        actor = layers.Dense(num_actions, name="actor")(hidden_common_layers[-1])
        critic = layers.Dense(1, name="critic")(hidden_common_layers[-1])

        model = keras.Model(inputs=inputs, outputs=[actor, critic])
        return model, "common" + "x".join([str(n) for n in common_layers])
    return create_model


def build_model_split(actor_layers=[128], critic_layers=[128]):
    def create_model(num_states, num_actions):
        inputs = layers.Input(shape=(num_states,), name="input")

        # Actor
        hidden_actor_layers = []
        for i, hidden_units in enumerate(actor_layers):
            previous = inputs if i == 0 else hidden_actor_layers[-1]
            hidden_actor_layers.append(layers.Dense(hidden_units, activation="relu", name=f"actor{i}")(previous))
        actor = layers.Dense(num_actions, name="actor")(hidden_actor_layers[-1])

        # Critic
        hidden_critic_layers = []
        for i, hidden_units in enumerate(critic_layers):
            previous = inputs if i == 0 else hidden_critic_layers[-1]
            hidden_critic_layers.append(layers.Dense(hidden_units, activation="relu", name=f"critic{i}")(previous))
        critic = layers.Dense(1, name="critic")(hidden_critic_layers[-1])

        model = keras.Model(inputs=inputs, outputs=[actor, critic])
        return model, "split" + "x".join([str(n) for n in actor_layers]) + "_" + "x".join([str(n) for n in critic_layers])
    return create_model


def run_spec(build_env, models, num_runs, render_freq):
    env, env_name = build_env()
    print(f"Evaluate models on {env_name}.")
    for i, build_model in enumerate(models):
        _, model_info = build_model(env.observation_space.shape[0], env.action_space.n)
        print(f"Evaluate model {i}: {model_info} ...")
        evaluate_model(build_env, build_model, num_runs, render_freq=render_freq)


# Environments
cartpole = build_cartpole()
dualpole = build_dualpole()
dualpole_touch = build_dualpole(end_on_touch=True)
dualpole_penalty = build_dualpole(dist_penalty=True)
dualpole_fadein = build_dualpole(fade_in_penalty=True)


# Model architectures
common16 = build_model_common(common_layers=[16])
common32 = build_model_common(common_layers=[32])
common64 = build_model_common(common_layers=[64])
common128 = build_model_common(common_layers=[128])
common192 = build_model_common(common_layers=[192])
common16x16 = build_model_common(common_layers=[16, 16])
common32x32 = build_model_common(common_layers=[32, 32])
common64x64 = build_model_common(common_layers=[64, 64])

split8_8 = build_model_split(actor_layers=[8], critic_layers=[8])
split10_10 = build_model_split(actor_layers=[10], critic_layers=[10])
split16_16 = build_model_split(actor_layers=[16], critic_layers=[16])
split24_24 = build_model_split(actor_layers=[24], critic_layers=[24])


# Test scenarios: (build_env, num_runs, [build_model])
cartpole_spec = (cartpole,
                 [
                     common16,
                     common32,
                     common64,
                     common16x16,
                     common32x32,
                     split8_8,
                     split10_10,
                     split16_16
                 ], 15, 200)

dualpole_spec = (dualpole,
                 [
                     common128,
                     common192,
                     common64x64
                 ], 3, 500)

dualpole_touch_spec = (dualpole_touch, [common128], 3, 500)
dualpole_penalty_spec = (dualpole_penalty, [common128], 3, 500)
dualpole_fadein_spec = (dualpole_fadein, [common128], 3, 500)


if __name__ == "__main__":
    run_spec(*cartpole_spec)
    run_spec(*dualpole_spec)
    run_spec(*dualpole_touch_spec)
    run_spec(*dualpole_penalty_spec)
    run_spec(*dualpole_fadein_spec)
