"""
Utility functions for loading and saving data.
"""


import matplotlib.pyplot as plt
import numpy as np
import json


PLOT_COUNTER = 0
TIME_FORMAT = "%m-%d_%H_%M_%S"


def load_results(env_name, model_name):
    summary_path = f"evaluation/{env_name}_{model_name}/summary.json"
    with open(summary_path) as file:
        summary = json.load(file)
    return summary["num_episodes_solved"]


def load_rewards(path):
    with open(f"{path}/history.json") as file:
        history_dict = json.load(file)
    history = History("", 100)
    rewards = history_dict["episodes"]
    avg_rewards = []
    for reward in rewards:
        history.add_reward(reward, 0)
        avg_rewards.append(history.get_avg_reward())

    return rewards, avg_rewards


def make_plot(values, title="", x_label="", y_label=""):
    global PLOT_COUNTER
    fig = plt.figure(PLOT_COUNTER)
    plt.title(title)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.plot(list(range(len(values))), values)
    PLOT_COUNTER += 1
    return fig


class History:

    def __init__(self, name, avg_n, model_info=""):
        self.name = name
        self.avg_n = avg_n
        self.model_info = model_info
        self.rewards = []
        self.durations = []

    def add_reward(self, reward, duration):
        self.rewards.append(reward)
        self.durations.append(duration)

    def get_avg_reward(self):
        return np.mean(self.rewards[-self.avg_n:])

    def save(self, directory, show_plots=True):

        json_str = json.dumps({
            "model_info": self.model_info,
            "episodes": self.rewards,
            "durations": self.durations
        }, indent=4)

        # save summary
        with open(f"{directory}/history.json", "w") as file:
            file.write(json_str)

        # save plots of reward and average reward

        # compute average reward
        avg_reward = []
        for t in range(len(self.rewards)):
            avg_reward.append(np.mean(self.rewards[max(0, t-self.avg_n):t+1]))

        rewards_fig = make_plot(self.rewards, title="reward during training", x_label="episode", y_label="reward")
        avg_rewards_fig = make_plot(avg_reward, title=f"average reward of last {self.avg_n} episodes during training", x_label="episode", y_label=f"avg{self.avg_n} reward")
        rewards_fig.savefig(f"{directory}/rewards.png")
        avg_rewards_fig.savefig(f"{directory}/avg_rewards.png")

        if show_plots:
            plt.show()

        plt.close("all")

    def get_avg_rewards(self):
        history2 = History("", 100)
        avg_rewards = []
        for reward in self.rewards:
            history2.add_reward(reward, 0)
            avg_rewards.append(history2.get_avg_reward())
        return avg_rewards