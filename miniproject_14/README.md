# Miniproject 14

Studying Actor Critic approach on CartPole and DualPole.

* `presentation.ipynb`: Notebook summarizing results. 
* `actor_critic.py`: Actor Critic implementation. 
* `dualpole.py`: Custom gym environment for controlling two carts simultaneously. 
* `evaluation/`: Results for various model architectures on CartPole and DualPole.