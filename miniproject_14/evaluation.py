"""
Training models and evaluate the performance.
"""


import tensorflow as tf
import tqdm
import json
import os
from tensorflow.keras.utils import plot_model
from datetime import datetime

from actor_critic import get_train_step
from visualization import render_episode, save_gif
from util import History, TIME_FORMAT


# Solved definition
avg_n = 100
avg_threshold = 475


# Training
MAX_EPISODES = 30000
MAX_STEPS = 500
optimizer = tf.keras.optimizers.Adam(learning_rate=0.01)
gamma = 0.99


def train_model(build_env, build_model, render_freq=200, base_dir="results"):

    env, env_name = build_env()
    model, model_info = build_model(env.observation_space.shape[0], env.action_space.n)

    directory = f"{base_dir}/{env_name}_{datetime.now().strftime(TIME_FORMAT)}"
    os.makedirs(directory)
    os.makedirs(f"{directory}/gifs")

    # generate a new tensorflow graph for each model
    train_step = get_train_step(env)

    history = History(env_name, avg_n, model_info=model_info)

    with tqdm.trange(MAX_EPISODES) as t:
        for episode in t:

            start_time = datetime.now()

            episode_reward = train_step(model, optimizer, gamma, MAX_STEPS)

            duration = (datetime.now() - start_time).total_seconds()

            history.add_reward(episode_reward, duration)
            avg_reward = history.get_avg_reward()

            t.set_description(f'Episode {episode}')
            t.set_postfix({"episode_reward": episode_reward, f"avg{avg_n}_reward": avg_reward})

            if episode != 0 and episode % render_freq == 0:
                temp_env, _ = build_env()
                images = render_episode(temp_env, model, MAX_STEPS)
                save_gif(f"{directory}/gifs/episode{episode}.gif", images)

            if avg_reward > avg_threshold:
                break

    history.save(directory, show_plots=False)
    save_gif(f"{directory}/gifs/episode{episode}_solved.gif", render_episode(env, model, 500))

    model.save(f"{directory}/model")

    return model, history


def evaluate_model(build_env, build_model, num_runs, render_freq=200):

    env, env_name = build_env()
    model, model_info = build_model(env.observation_space.shape[0], env.action_space.n)
    evaluation_dir = f"evaluation/{env_name}_{model_info}_{datetime.now().strftime(TIME_FORMAT)}"
    os.makedirs(evaluation_dir)

    plot_model(model, to_file=f"{evaluation_dir}/model.png", show_shapes=True, show_layer_names=True)

    results = [train_model(build_env, build_model, render_freq=render_freq, base_dir=evaluation_dir) for _ in range(num_runs)]

    json_str = json.dumps({
        "env": env_name,
        "model_info": model_info,
        "num_episodes_solved": [len(result[1].rewards) for result in results]
    }, indent=4)

    # save summary
    with open(f"{evaluation_dir}/summary.json", "w") as file:
        file.write(json_str)


    return results


