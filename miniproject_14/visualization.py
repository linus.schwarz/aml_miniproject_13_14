"""
Functions for plotting all kinds of training results.
"""


import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
import imageio


colors = ["c", "m", "y", "r", "g", "b", "k"] * 5
colors_rgb = ["r", "g", "b", "c", "m", "y", "k"] * 5

TITLE_SIZE = 16
YLABEL_SIZE = 14
XLABEL_SIZE = 14
X_SIZE = 12
Y_SIZE = 12


def render_episode(env, model, max_steps, continue_done=False):
    """
    Render an episode using actions from the specified model and return the images.
    """
    images = []
    state = tf.constant(env.reset(), dtype=tf.float32)
    for i in range(1, max_steps + 1):

        image = env.render(mode='rgb_array')
        if image is not None:
            images.append(image)

        state = tf.expand_dims(state, 0)
        if model is None:
            action = env.action_space.sample()
        else:
            action_probs, _ = model(state)
            action = np.argmax(np.squeeze(action_probs))

        state, _, done, _ = env.step(action)
        state = tf.constant(state, dtype=tf.float32)
        if done and not continue_done:
            break

    env.close()

    return images


def save_gif(filepath, images):
    """
    Save a given sequence of images as a gif.
    """
    imageio.mimsave(filepath, images, fps=50)


def plot_rewards(list_of_rewards, labels=None, title="", figsize=(10,2), rgb_colors=False):


    local_colors = colors_rgb if rgb_colors else colors

    fig = plt.figure(figsize=figsize)

    for i, rewards in enumerate(list_of_rewards):
        label = "" if labels is None else labels[i]
        x = np.arange(len(rewards))
        plt.plot(x, rewards, label=label, color=local_colors[i])

    max_len = max([len(rewards) for rewards in list_of_rewards])
    plt.plot(np.full((max_len), 475), 'k:')

    if labels is not None:
        plt.legend(loc="upper left")

    plt.xlabel("episode", fontsize=XLABEL_SIZE)
    plt.ylabel("reward", fontsize=YLABEL_SIZE)
    plt.title(title, fontsize=TITLE_SIZE)
    plt.show()


def plot_bins(model_name, model_rewards, bin_size, num_bins, title=""):
    """
    Plot the timesteps to solve for the given scenarios.
    """

    bar = [0 for _ in range(num_bins + 1)]
    bar_labels = [f"<{bin_size * (i+1)}" for i in range(num_bins)] + [f">{bin_size * num_bins}"]
    for reward in model_rewards:
        bin = int(reward / bin_size)
        if bin >= num_bins:
            bin = num_bins
        bar[bin] += 1


    fig = plt.figure(figsize=(10, 2))
    ax = fig.add_axes([0, 0, 1, 1])
    ax.bar(bar_labels, bar, label=model_name)

    plt.legend(loc="upper left")
    plt.ylabel("num_solved", fontsize=YLABEL_SIZE)
    plt.xlabel("training iterations", fontsize=XLABEL_SIZE)
    plt.xticks(fontsize=X_SIZE)
    plt.yticks(fontsize=Y_SIZE)
    plt.title(title, fontsize=TITLE_SIZE)

    plt.show()


def plot_mean(model_names, model_results, title="", sort_means=True):
    model_means = [np.mean(rewards) for rewards in model_results]
    sort_key = lambda name_mean: name_mean[1] if sort_means else None
    sorted_names_means = sorted(zip(model_names, model_means), key=sort_key)

    labels = [name for name, _ in sorted_names_means]
    values = [mean for _, mean in sorted_names_means]

    fig = plt.figure(figsize=(14,2))
    ax = fig.add_axes([0, 0, 1, 1])
    ax.bar(labels, values)
    plt.ylabel("mean number of episodes", fontsize=YLABEL_SIZE)
    plt.xticks(fontsize=X_SIZE)
    plt.yticks(fontsize=Y_SIZE)
    plt.title(title, fontsize=TITLE_SIZE)
    plt.show()

