"""
Extension of the classic CartPole environment (https://github.com/openai/gym/blob/master/gym/envs/classic_control/cartpole.py) to a two-pole system.
"""

import math
import gym
import numpy as np

from gym import spaces, logger
from gym.utils import seeding


class Cart(object):
    """
    A simulated cart.
    """

    def __init__(self):
        self.state = None

        # Physics stuff.
        self.gravity = 9.8
        self.masscart = 1.0
        self.masspole = 0.1
        self.total_mass = (self.masspole + self.masscart)
        self.length = 0.5  # actually half the pole's length
        self.polemass_length = (self.masspole * self.length)
        self.force_mag = 10.0
        self.tau = 0.02  # seconds between state updates

    def apply_action(self, action):
        # The current state.
        x, x_dot, theta, theta_dot = self.state
        force = self.force_mag if action == 1 else -self.force_mag
        costheta = math.cos(theta)
        sintheta = math.sin(theta)

        # Prepare integration step.
        temp = (force + self.polemass_length * theta_dot ** 2 * sintheta) / self.total_mass
        thetaacc = (self.gravity * sintheta - costheta * temp) / (self.length * (4.0 / 3.0 - self.masspole * costheta ** 2 / self.total_mass))
        xacc = temp - self.polemass_length * thetaacc * costheta / self.total_mass

        # Perform euler integration.
        x = x + self.tau * x_dot
        x_dot = x_dot + self.tau * xacc
        theta = theta + self.tau * theta_dot
        theta_dot = theta_dot + self.tau * thetaacc

        # Set the new state.
        self.state = np.array([x, x_dot, theta, theta_dot])
        return self.state


class DualPole(gym.Env):
    """
    Two pole version of the classic CartPole environment.

    Observation:
        The concatenated states of both carts.
        Type: Box(8)
    Actions:
        The combined action space mapped to the individual actions using 'self.action_encoding'.
        Type: Discrete(4)
    """

    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 50
    }

    def __init__(self, end_on_touch=False, dist_penalty=False, fade_in_penalty=False):

        self.elapsed_episodes = 0

        self.end_on_touch = end_on_touch
        self.dist_penalty = dist_penalty or fade_in_penalty
        self.fade_in_penalty = fade_in_penalty

        # Minimum distance allowed between the two carts (if end_on_touch=True)
        self.touch_dist = 0.5

        # If distance below, add a distance penalty to the reward (dist_penalty=True or fade_in_penalty=True)
        self.penalty_dist = 0.5
        self.penalty = 1
        self.penalty_ratio = 0

        # Fade in the distance penalty between these episodes (fade_in_penalty=True)
        self.fade_start_episode = 1000
        self.fade_end_episode = 6000


        # Angle at which to fail the episode
        self.theta_threshold_radians = 12 * 2 * math.pi / 360
        self.x_threshold = 2.4

        # Mapping to the individual actions.
        self.action_encoding = {
            0: (0, 0),
            1: (0, 1),
            2: (1, 0),
            3: (1, 1)
        }
        # Combined action space.
        self.action_space = spaces.Discrete(4)

        # Construct the combined bounds.
        high = np.array([self.x_threshold * 2,
                         np.finfo(np.float32).max,
                         self.theta_threshold_radians * 2,
                         np.finfo(np.float32).max] * 2,
                        dtype=np.float32)
        # Use concatenated bounds to construct combined observation space.
        self.observation_space = spaces.Box(-high, high, dtype=np.float32)

        self.seed()
        self.viewer = None
        self.state = None

        self.steps_beyond_done = None

        # The initial offset of the two carts from the coordinate center.
        self.cart_offset = 1
        self.generate_initial_state = lambda left: \
            self.np_random.uniform(low=-0.05, high=0.05, size=(4,)) \
            + (-1 if left else 1) * np.array([self.cart_offset, 0, 0, 0])

        # Instantiate two simulated carts.
        self.cart1 = Cart()
        self.cart2 = Cart()

        # Track the amount of steps for termination.
        self.max_episode_steps = 500
        self.elapsed_steps = None

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def step(self, action):
        # Check if action is valid.
        assert self.action_space.contains(action), "Invalid action!"

        self.elapsed_steps += 1

        # Perform actions for both simulated carts.
        action1, action2 = self.action_encoding[action]
        state1 = self.cart1.apply_action(action1)
        state2 = self.cart2.apply_action(action2)
        self.state = np.concatenate([state1, state2])

        # Check if one of the carts failed, i.e. one of state values is outside the bounds.
        # The environment is "done" if one the carts failed.
        x1, theta1, x2, theta2 = self.state[[0, 2, 4, 6]]
        done = bool(
            x1 < -self.x_threshold or x2 < -self.x_threshold
            or x1 > self.x_threshold or x2 > self.x_threshold
            or theta1 < -self.theta_threshold_radians or theta2 < -self.theta_threshold_radians
            or theta1 > self.theta_threshold_radians or theta2 > self.theta_threshold_radians
        )
        # If self.end_on_touch=True, end the episode if the carts are too close to each other.
        done = done or (self.end_on_touch and x1 + self.touch_dist > x2)
        # The environment is "done" after a maximum number of steps is reached.
        done = done or self.elapsed_steps >= self.max_episode_steps

        # The classic reward.
        reward = 1.0

        # Check if distance penalty should be applied to reward
        dist = x2 - x1
        if self.dist_penalty and dist < self.penalty_dist:
            if self.fade_in_penalty:
                # Interpolate impact of distance penalty during training from 0 to 1 with to the current ratio
                reward -= self.penalty_ratio * self.penalty
            else:
                reward -= self.penalty

        if not done:
            # Just use the calculated reward
            pass
        elif self.steps_beyond_done is None:
            self.steps_beyond_done = 0
        else:
            if self.steps_beyond_done == 0:
                logger.warn(
                    "You are calling 'step()' even though this "
                    "environment has already returned done = True. You "
                    "should always call 'reset()' once you receive 'done = "
                    "True' -- any further steps are undefined behavior."
                )
            self.steps_beyond_done += 1
            reward = 0.0

        return self.state, reward, done, {}

    def reset(self):
        self.cart1.state = self.generate_initial_state(True)
        self.cart2.state = self.generate_initial_state(False)
        self.steps_beyond_done = None
        self.elapsed_steps = 0
        if self.fade_in_penalty:
            # Calculate the current penalty_ratio for the next training episode
            ratio = (self.elapsed_episodes - self.fade_start_episode) / (self.fade_end_episode - self.fade_start_episode)
            self.penalty_ratio = max(0.0, min(1.0, ratio))
        self.elapsed_episodes += 1
        return np.concatenate([self.cart1.state, self.cart2.state])

    def render(self, mode="human"):
        screen_width = 600
        screen_height = 400

        length = 0.5
        world_width = self.x_threshold * 2
        scale = screen_width/world_width
        carty = 100  # TOP OF CART
        polewidth = 10.0
        polelen = scale * (2 * length)
        cartwidth = 50.0
        cartheight = 30.0

        if self.viewer is None:
            # Initialize
            from gym.envs.classic_control import rendering
            self.viewer = rendering.Viewer(screen_width, screen_height)
            l, r, t, b = -cartwidth / 2, cartwidth / 2, cartheight / 2, -cartheight / 2
            axleoffset = cartheight / 4.0

            # Carts
            cart1_polygon = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            self.carttrans1 = rendering.Transform()
            cart1_polygon.add_attr(self.carttrans1)
            self.viewer.add_geom(cart1_polygon)

            cart2_polygon = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            self.carttrans2 = rendering.Transform()
            cart2_polygon.add_attr(self.carttrans2)
            self.viewer.add_geom(cart2_polygon)

            # Poles
            l, r, t, b = -polewidth / 2, polewidth / 2, polelen - polewidth / 2, -polewidth / 2
            pole1 = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            pole1.set_color(.8, .6, .4)
            self.poletrans1 = rendering.Transform(translation=(0, axleoffset))
            pole1.add_attr(self.poletrans1)
            pole1.add_attr(self.carttrans1)
            self.viewer.add_geom(pole1)

            pole2 = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            pole2.set_color(.8, .6, .4)
            self.poletrans2 = rendering.Transform(translation=(0, axleoffset))
            pole2.add_attr(self.poletrans2)
            pole2.add_attr(self.carttrans2)
            self.viewer.add_geom(pole2)

            # Axles
            self.axle1 = rendering.make_circle(polewidth / 2)
            self.axle1.add_attr(self.poletrans1)
            self.axle1.add_attr(self.carttrans1)
            self.axle1.set_color(.5, .5, .8)
            self.viewer.add_geom(self.axle1)

            self.axle2 = rendering.make_circle(polewidth / 2)
            self.axle2.add_attr(self.poletrans2)
            self.axle2.add_attr(self.carttrans2)
            self.axle2.set_color(.5, .5, .8)
            self.viewer.add_geom(self.axle2)

            # Track
            self.track = rendering.Line((0, carty), (screen_width, carty))
            self.track.set_color(0, 0, 0)
            self.viewer.add_geom(self.track)

            self._pole_geom1 = pole1
            self._pole_geom2 = pole2

        if self.state is None:
            return None

        # Edit the pole polygon vertex
        pole1 = self._pole_geom1
        l, r, t, b = -polewidth / 2, polewidth / 2, polelen - polewidth / 2, -polewidth / 2
        pole1.v = [(l, b), (l, t), (r, t), (r, b)]

        pole2 = self._pole_geom2
        l, r, t, b = -polewidth / 2, polewidth / 2, polelen - polewidth / 2, -polewidth / 2
        pole2.v = [(l, b), (l, t), (r, t), (r, b)]

        # Set transforms according to states
        x1 = self.state[0]
        rot1 = self.state[2]
        cartx1 = x1 * scale + screen_width / 2.0  # MIDDLE OF CART
        self.carttrans1.set_translation(cartx1, carty)
        self.poletrans1.set_rotation(-rot1)

        x2 = self.state[4]
        rot2 = self.state[6]
        cartx2 = x2 * scale + screen_width / 2.0  # MIDDLE OF CART
        self.carttrans2.set_translation(cartx2, carty)
        self.poletrans2.set_rotation(-rot2)

        return self.viewer.render(return_rgb_array=mode == 'rgb_array')

    def close(self):
        if self.viewer:
            self.viewer.close()
            self.viewer = None
















